package com.gitrekt.quora;

import com.gitrekt.quora.commands.handlers.RecommendedTopicsCommand;
import com.gitrekt.quora.database.postgres.handlers.RecommendedTopicsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RecommendTopicsCommandTest {
  protected RecommendedTopicsPostgresHandler handler;
  protected RecommendedTopicsCommand command;
  protected HashMap<String, Object> args;

  @Before
  public void init() {
    args = new HashMap<>();
    args.put("userId", new JsonPrimitive("46eb6050-2aa8-434d-b65b-05b4eeccb1d5"));

    handler = new RecommendedTopicsPostgresHandler();

    command = new RecommendedTopicsCommand(args);
    command.setPostgresHandler(handler);
  }

  @Test
  public void testRecommendedTopics() throws BadRequestException {
    JsonArray res = command.execute().getAsJsonArray("Recommended Topics").getAsJsonArray();

    assertEquals(
        "Recommended Topics count is 2, Only topics that followed users are subscribed will "
            + "be recommended ",
        res.size(),
        2);

    JsonObject first = res.get(0).getAsJsonObject();
    JsonObject second = res.get(1).getAsJsonObject();

    assertEquals(
        "Common topics between followed users are shown first",
        first.get("topicId").toString().replaceAll("\"", ""),
        "44fde97a-5cf8-11e9-8647-d663bd873d93");
    assertEquals(
        "Less Common topics between followed users are shown last",
        second.get("topicId").toString().replaceAll("\"", ""),
        "aacec07e-5ceb-11e9-8647-d663bd873d93");

    assertNotNull("No attribute should be null in the model", first.get("topicName").toString().replaceAll("\"", ""));
    assertNotNull("No attribute should be null in the model", second.get("topicName").toString().replaceAll("\"", ""));
  }
}
