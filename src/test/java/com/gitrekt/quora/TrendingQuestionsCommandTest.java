package com.gitrekt.quora;

import com.gitrekt.quora.commands.handlers.RecommendedTopicsCommand;
import com.gitrekt.quora.commands.handlers.TrendingQuestionsCommand;
import com.gitrekt.quora.database.postgres.handlers.RecommendedTopicsPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.TrendingQuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.RecommendedTopics;
import com.gitrekt.quora.models.TrendingQuestions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TrendingQuestionsCommandTest {
  protected TrendingQuestionsPostgresHandler handler;
  protected TrendingQuestionsCommand command;
  protected HashMap<String, Object> args;

  @Before
  public void init() {
    args = new HashMap<>();
    args.put("userId", new JsonPrimitive("46eb6050-2aa8-434d-b65b-05b4eeccb1d5"));
    args.put("topicId", new JsonPrimitive("aacebd04-5ceb-11e9-8647-d663bd873d93"));

    handler = new TrendingQuestionsPostgresHandler();

    command = new TrendingQuestionsCommand(args);
    command.setPostgresHandler(handler);
  }

  @Test
  public void testTrendingQuestions() throws BadRequestException {
    JsonArray res = command.execute().get("Trending Questions").getAsJsonArray();

    assertEquals(
        "Trending Questions count is 4, Only not deleted questions in topic requested will "
            + "be recommended ",
        res.size(),
        4);

    JsonObject first = res.get(0).getAsJsonObject();
    JsonObject second = res.get(1).getAsJsonObject();
    JsonObject third = res.get(2).getAsJsonObject();
    JsonObject fourth = res.get(3).getAsJsonObject();

    assertEquals(
        "Common topics between followed users are shown first",
        first.get("questionId").toString().replaceAll("\"", ""),
        "3d45982c-5d07-11e9-8647-d663bd873d93");
    assertEquals(
        "Less Common topics between followed users are shown last",
        second.get("questionId").toString().replaceAll("\"", ""),
        "3d459598-5d07-11e9-8647-d663bd873d93");
    assertEquals(
        "Less Common topics between followed users are shown last",
        third.get("questionId").toString().replaceAll("\"", ""),
        "3d4590a2-5d07-11e9-8647-d663bd873d93");
    assertEquals(
        "Less Common topics between followed users are shown last",
        fourth.get("questionId").toString().replaceAll("\"", ""),
        "3d458d78-5d07-11e9-8647-d663bd873d93");

    assertNotNull(
        "No attribute should be null in the model", first.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull("No attribute should be null in the model", first.get("body").toString().replaceAll("\"", ""));

    assertNotNull(
        "No attribute should be null in the model", second.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model", second.get("body").toString().replaceAll("\"", ""));

    assertNotNull(
        "No attribute should be null in the model", third.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull("No attribute should be null in the model", third.get("body").toString().replaceAll("\"", ""));

    assertNotNull(
        "No attribute should be null in the model", fourth.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model", fourth.get("body").toString().replaceAll("\"", ""));
  }
}
