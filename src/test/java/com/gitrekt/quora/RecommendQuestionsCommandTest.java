package com.gitrekt.quora;

import com.gitrekt.quora.commands.handlers.RecommendedQuestionsCommand;
import com.gitrekt.quora.database.postgres.handlers.RecommendedQuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RecommendQuestionsCommandTest {
  protected RecommendedQuestionsPostgresHandler handler;
  protected RecommendedQuestionsCommand command;
  protected HashMap<String, Object> args;

  @Before
  public void init() {
    args = new HashMap<>();
    args.put("userId", new JsonPrimitive("46eb6050-2aa8-434d-b65b-05b4eeccb1d5"));

    handler = new RecommendedQuestionsPostgresHandler();

    command = new RecommendedQuestionsCommand(args);
    command.setPostgresHandler(handler);
  }

  @Test
  public void testRecommendedQuestions() throws BadRequestException {
    JsonArray res = command.execute().getAsJsonArray("Recommended Questions");

    assertEquals(
        "Recommended Questions count is 2, Only questions in topics user follows is recommended",
        res.size(),
        2);

    JsonObject first = res.get(0).getAsJsonObject();
    JsonObject second = res.get(1).getAsJsonObject();

    assertEquals(
        "Latest Questions is recommended first",
        first.get("questionId").toString().replaceAll("\"", ""),
        "3d45982c-5d07-11e9-8647-d663bd873d93");
    assertEquals(
        "Older Questions comes last first",
        second.get("questionId").toString().replaceAll("\"", ""),
        "3d459598-5d07-11e9-8647-d663bd873d93");

    assertNotNull(
        "No attribute should be null in the model",
        first.get("body").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        first.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        first.get("topicId").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        first.get("topicName").toString().replaceAll("\"", ""));

    assertNotNull(
        "No attribute should be null in the model",
        second.get("body").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        second.get("questionTitle").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        second.get("topicId").toString().replaceAll("\"", ""));
    assertNotNull(
        "No attribute should be null in the model",
        second.get("topicName").toString().replaceAll("\"", ""));
  }
}
