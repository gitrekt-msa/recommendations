package com.gitrekt.quora;

import com.gitrekt.quora.commands.handlers.RecommendedUsersCommand;
import com.gitrekt.quora.database.postgres.handlers.RecommendedUsersPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RecommendUsersCommandTest {
  protected RecommendedUsersPostgresHandler handler;
  protected RecommendedUsersCommand command;
  protected HashMap<String, Object> args;

  @Before
  public void init() {
    args = new HashMap<>();
    args.put("userId", new JsonPrimitive("46eb6050-2aa8-434d-b65b-05b4eeccb1d5"));

    handler = new RecommendedUsersPostgresHandler();

    command = new RecommendedUsersCommand(args);
    command.setPostgresHandler(handler);
  }

  @Test
  public void testRecommendedUsers() throws BadRequestException {
    JsonArray res = command.execute().get("Recommended Users").getAsJsonArray();

    assertEquals(
        "Users with 50% or more of common subscribed topics will be recommended", res.size(), 1);

    JsonObject first = res.get(0).getAsJsonObject();

    assertEquals(
        "Only user with 50% or more will be shown",
        first.get("userId").toString().replaceAll("\"", ""), "aaceaf3a-5ceb-11e9-8647-d663bd873d93");

    assertNotNull("No attribute should be null in the model", first.get("firstName").toString().replaceAll("\"", ""));
    assertNotNull("No attribute should be null in the model", first.get("lastName").toString().replaceAll("\"", ""));
  }
}
