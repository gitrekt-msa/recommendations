package com.gitrekt.quora.controller;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.postgres.handlers.PostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;

// Would be better to store all commands in a Map commandName -> Command
public final class Invoker {

  private static final Config CONFIG = Config.getInstance();

  /**
   * . Invoker to apply reflection on commands
   * @param commandName a String representing command name
   * @param arguments hashmap of arguments that commands need to execute
   * @return Result of executing the command
   * @throws ClassNotFoundException Class Not Found Exception
   * @throws NoSuchMethodException No Such Method Exception
   * @throws IllegalAccessException Illegal Access Exception
   * @throws InvocationTargetException Invocation Target Exception
   * @throws InstantiationException Instantiation Exception
   * @throws SQLException SQL Exception
   * @throws BadRequestException Bad Request Exception
   */
  public static Object invoke(String commandName, HashMap<String, Object> arguments)
      throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
          InvocationTargetException, InstantiationException, SQLException, BadRequestException {

    String commandClassPath =
        CONFIG.getProperty("commandsPackage") + "." + CONFIG.getProperty(commandName);
    Class<?> commandClass = getClass(commandClassPath);
    Constructor<?> commandConstructor = getConstructor(commandClass, HashMap.class);

    String postgresClassPath =
        CONFIG.getProperty("postgresPackage")
            + "."
            + CONFIG.getProperty(commandName + ".dbhandler");
    Constructor<?> postgresConstructor = getConstructor(getClass(postgresClassPath));

    Command command = (Command) commandConstructor.newInstance(arguments);

    command.setPostgresHandler((PostgresHandler) postgresConstructor.newInstance());

    return command.execute();
  }

  private static Class<?> getClass(String classPath) throws ClassNotFoundException {
    return Class.forName(classPath);
  }

  private static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes)
      throws NoSuchMethodException {

    return clazz.getConstructor(parameterTypes);
  }
}
