package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.RecommendedTopics;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class RecommendedTopicsPostgresHandler extends PostgresHandler<RecommendedTopics> {

  public RecommendedTopicsPostgresHandler() {
    super(null, RecommendedTopics.class);
  }

  /**
   * . Recommended topics for a user to subscribe to
   * @param userId user id that will be recommended the topics
   * @return list of recommended topics
   */
  public List<RecommendedTopics> getRecommendedTopics(String userId) {

    try {
      ResultSetHandler<List<RecommendedTopics>> elements =
          new BeanListHandler<RecommendedTopics>(
              mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<RecommendedTopics> recommendedTopics =
          runner.query(
              String.format(
                  "SELECT * FROM Get_Recommended_Topics('" + UUID.fromString(userId) + "')"),
              elements);
      return recommendedTopics.stream().collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }
}
