package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.RecommendedQuestions;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class RecommendedQuestionsPostgresHandler extends PostgresHandler<RecommendedQuestions> {

  public RecommendedQuestionsPostgresHandler() {
    super(null, RecommendedQuestions.class);
  }

  /**. Returns recommended questions to answer for specific user
   * @param userId user id initiating request
   * @return list of unanswered questions
   */
  public List<RecommendedQuestions> getRecommendedQuestions(String userId) {

    try {
      ResultSetHandler<List<RecommendedQuestions>> elements =
          new BeanListHandler<RecommendedQuestions>(
              mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<RecommendedQuestions> recommendedQuestions =
          runner.query(
              String.format(
                  "SELECT * FROM Get_Recommended_Questions('" + UUID.fromString(userId) + "')"),
              elements);
      return recommendedQuestions.stream().collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }
}
