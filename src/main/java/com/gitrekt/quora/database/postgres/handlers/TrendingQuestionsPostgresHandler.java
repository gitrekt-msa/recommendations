package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.TrendingQuestions;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;



public class TrendingQuestionsPostgresHandler extends PostgresHandler<TrendingQuestions> {

  public TrendingQuestionsPostgresHandler() {
    super(null, TrendingQuestions.class);
  }

  /**
   * . Trending questions in a specific topic
   * @param topicId topic id that will be returning its trending questions
   * @return list of trending questions
   */
  public List<TrendingQuestions> getTrendingQuestions(String topicId) {

    try {
      ResultSetHandler<List<TrendingQuestions>> elements =
          new BeanListHandler<TrendingQuestions>(
              mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<TrendingQuestions> trendingQuestions =
          runner.query(
              String.format("SELECT * FROM Get_Trending_Questions('" + UUID.fromString(topicId)
                  + "')"), elements);
      return trendingQuestions.stream().collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }
}
