package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.RecommendedUsers;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class RecommendedUsersPostgresHandler extends PostgresHandler<RecommendedUsers> {

  public RecommendedUsersPostgresHandler() {
    super(null, RecommendedUsers.class);
  }

  /**
   * . Recommended users for a user to follow
   * @param userId users id that the system recommend users for
   * @return list of recommended users
   */
  public List<RecommendedUsers> getRecommendedUsers(String userId) {

    try {
      ResultSetHandler<List<RecommendedUsers>> elements =
          new BeanListHandler<RecommendedUsers>(
              mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<RecommendedUsers> recommendedUsers =
          runner.query(
              String.format(
                  "SELECT * FROM Get_Recommended_Users('" + UUID.fromString(userId) + "')"),
              elements);
      return recommendedUsers.stream().collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }
}
