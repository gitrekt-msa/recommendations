package com.gitrekt.quora.commands;

import com.gitrekt.quora.database.arango.handlers.ArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.PostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.util.Arrays;
import java.util.HashMap;

public abstract class Command {

  protected HashMap<String, Object> args;

  protected PostgresHandler<?> postgresHandler;

  private ArangoHandler<?> arangoHandler;

  public Command(HashMap<String, Object> args) {
    this.args = args;
  }

  public void setPostgresHandler(PostgresHandler<?> postgresHandler) {
    this.postgresHandler = postgresHandler;
  }

  public void setArangoHandler(ArangoHandler<?> arangoHandler) {
    this.arangoHandler = arangoHandler;
  }

  public abstract Object execute() throws BadRequestException;

  protected void checkArguments(String[] requiredArgs) throws BadRequestException {
    StringBuilder stringBuilder = new StringBuilder();

    for (String argument : requiredArgs) {
      if (!args.containsKey(argument) || args.get(argument) == null) {
        stringBuilder.append(String.format("Argument %s is missing", argument)).append("\n");
      }
    }

    if (stringBuilder.length() > 0) {
      throw new BadRequestException(stringBuilder.toString());
    }
  }


}
