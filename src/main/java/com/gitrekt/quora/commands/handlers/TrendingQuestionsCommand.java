package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.TrendingQuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.TrendingQuestions;
import com.gitrekt.quora.redis.Redis;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.HashMap;
import java.util.List;

import redis.clients.jedis.Jedis;

public class TrendingQuestionsCommand extends Command {
  private static final String[] argumentNames = new String[] {"topicId"};
  private static final int keyTimeout = 7200; // equivalent to 2 hour

  public TrendingQuestionsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonObject execute() throws BadRequestException {
    checkArguments(argumentNames);
    String topicId = ((JsonPrimitive) args.get("topicId")).getAsString();

    try (Jedis redisClient = Redis.getInstance().getRedisPool().getResource()) {
      Gson gson = new Gson();

      if (redisClient.exists("topic:" + topicId + ":trending")) {
        return gson.fromJson(redisClient.get("topic:" + topicId + ":trending"), JsonObject.class);
      }

      List<TrendingQuestions> trendingQuestions =
          ((TrendingQuestionsPostgresHandler) postgresHandler).getTrendingQuestions(topicId);

      JsonObject res = new JsonObject();
      res.add("Trending Questions", gson.toJsonTree(trendingQuestions));

      redisClient.set("topic:" + topicId + ":trending", res.toString());
      redisClient.expire("topic:" + topicId + ":trending", keyTimeout);
      return res;
    }
  }
}
