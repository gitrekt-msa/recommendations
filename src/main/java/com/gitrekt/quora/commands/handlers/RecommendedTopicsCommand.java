package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.RecommendedTopicsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.RecommendedTopics;
import com.gitrekt.quora.redis.Redis;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.HashMap;
import java.util.List;

import redis.clients.jedis.Jedis;

public class RecommendedTopicsCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId"};
  private static final int keyTimeout = 3600; // equivalent to 1 hour

  public RecommendedTopicsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonObject execute() throws BadRequestException {
    checkArguments(argumentNames);
    String userId = ((JsonPrimitive) args.get("userId")).getAsString();
    try (Jedis redisClient = Redis.getInstance().getRedisPool().getResource()) {
      Gson gson = new Gson();

      if (redisClient.exists("user:" + userId + ":topics")) {
        return gson.fromJson(redisClient.get("user:" + userId + ":topics"), JsonObject.class);
      }

      List<RecommendedTopics> recommendedTopics =
          ((RecommendedTopicsPostgresHandler) postgresHandler).getRecommendedTopics(userId);

      JsonObject res = new JsonObject();
      res.add("Recommended Topics", gson.toJsonTree(recommendedTopics));

      redisClient.set("user:" + userId + ":topics", res.toString());
      redisClient.expire("user:" + userId + ":topics", keyTimeout);
      return res;
    }
  }
}
