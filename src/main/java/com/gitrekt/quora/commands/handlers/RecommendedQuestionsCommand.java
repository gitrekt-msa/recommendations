package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.RecommendedQuestionsPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.RecommendedQuestions;
import com.gitrekt.quora.redis.Redis;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.HashMap;
import java.util.List;

import redis.clients.jedis.Jedis;

public class RecommendedQuestionsCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId"};
  private static final int keyTimeout = 300; // equivalent to 5 minutes

  public RecommendedQuestionsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonObject execute() throws BadRequestException {
    checkArguments(argumentNames);
    String userId = ((JsonPrimitive) args.get("userId")).getAsString();

    try (Jedis redisClient = Redis.getInstance().getRedisPool().getResource()) {
      Gson gson = new Gson();

      if (redisClient.exists("user:" + userId + ":questions")) {
        return gson.fromJson(redisClient.get("user:" + userId + ":questions"), JsonObject.class);
      }

      List<RecommendedQuestions> recommendedQuestions =
          ((RecommendedQuestionsPostgresHandler) postgresHandler).getRecommendedQuestions(userId);

      JsonObject res = new JsonObject();
      res.add("Recommended Questions", gson.toJsonTree(recommendedQuestions));

      redisClient.set("user:" + userId + ":questions", res.toString());
      redisClient.expire("user:" + userId + ":questions", keyTimeout);
      return res;
    }
  }
}
