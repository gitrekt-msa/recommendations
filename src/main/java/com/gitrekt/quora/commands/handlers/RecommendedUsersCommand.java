package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.RecommendedUsersPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.gitrekt.quora.models.RecommendedUsers;
import com.gitrekt.quora.redis.Redis;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.HashMap;
import java.util.List;

import redis.clients.jedis.Jedis;

public class RecommendedUsersCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId"};
  private static final int keyTimeout = 3600; // equivalent to 1 hour

  public RecommendedUsersCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonObject execute() throws BadRequestException {
    checkArguments(argumentNames);
    String userId = ((JsonPrimitive) args.get("userId")).getAsString();

    try (Jedis redisClient = Redis.getInstance().getRedisPool().getResource()) {
      Gson gson = new Gson();

      if (redisClient.exists("user:" + userId + ":users")) {
        return gson.fromJson(redisClient.get("user:" + userId + ":users"), JsonObject.class);
      }

      List<RecommendedUsers> recommendedUsers =
          ((RecommendedUsersPostgresHandler) postgresHandler).getRecommendedUsers(userId);

      JsonObject res = new JsonObject();
      res.add("Recommended Users", gson.toJsonTree(recommendedUsers));

      redisClient.set("user:" + userId + ":users", res.toString());
      redisClient.expire("user:" + userId + ":users", keyTimeout);

      return res;
    }
  }
}
