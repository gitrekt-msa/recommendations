package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.RecommendedQuestionsPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.RecommendedTopicsPostgresHandler;
import com.gitrekt.quora.database.postgres.handlers.RecommendedUsersPostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class AllRecommendationsCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId"};
  private static RecommendedQuestionsCommand questionsCommand;
  private static RecommendedTopicsCommand topicsCommand;
  private static RecommendedUsersCommand usersCommand;

  /**.
   * Command for generating all recommendations displayed on wall
   * @param args hashmap representing command arguments
   */
  public AllRecommendationsCommand(HashMap<String, Object> args) {
    super(args);
    questionsCommand = new RecommendedQuestionsCommand(args);
    questionsCommand.setPostgresHandler(new RecommendedQuestionsPostgresHandler());

    topicsCommand = new RecommendedTopicsCommand(args);
    topicsCommand.setPostgresHandler(new RecommendedTopicsPostgresHandler());

    usersCommand = new RecommendedUsersCommand(args);
    usersCommand.setPostgresHandler(new RecommendedUsersPostgresHandler());
  }

  @Override
  public JsonObject execute() throws BadRequestException {
    checkArguments(argumentNames);
    JsonObject res = new JsonObject();
    JsonElement questions = questionsCommand.execute().get("Recommended Questions");
    JsonElement topics = topicsCommand.execute().get("Recommended Topics");
    JsonElement users = usersCommand.execute().get("Recommended Users");
    JsonArray emptyArray = new JsonArray();

    res.add("questions", questions.isJsonNull()? emptyArray: questions);
    res.add("topics", topics.isJsonNull() ? emptyArray: topics);
    res.add("users", users.isJsonNull() ? emptyArray: users);
    System.out.println(res);
    return res;
  }
}
