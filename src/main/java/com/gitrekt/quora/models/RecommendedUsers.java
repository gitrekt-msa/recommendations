package com.gitrekt.quora.models;

public class RecommendedUsers {
  private String userId;
  private String firstName;
  private String lastName;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lsatName) {
    this.lastName = lsatName;
  }

  @Override
  public String toString() {
    return "RecommendedUsers{"
        + "userId='"
        + userId
        + '\''
        + ", first_name='"
        + firstName
        + '\''
        + ", last_name='"
        + lastName
        + '\''
        + '}';
  }
}
