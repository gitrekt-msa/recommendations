package com.gitrekt.quora.models;

public class RecommendedQuestions {
  private String questionId; // question id
  private String questionTitle; // question title
  private String body; // question body
  private int upvotes; // question upvotes count
  private String topicId; // question topic id
  private String topicName; // topic name

  public String getQuestionId() {
    return questionId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public int getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(int upvotes) {
    this.upvotes = upvotes;
  }

  public String getTopicId() {
    return topicId;
  }

  public void setTopicId(String topicId) {
    this.topicId = topicId;
  }

  public String getTopicName() {
    return topicName;
  }

  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }

  @Override
  public String toString() {
    return "RecommendedQuestions{"
        + "questionId='"
        + questionId
        + '\''
        + ", title='"
        + questionTitle
        + '\''
        + ", body='"
        + body
        + '\''
        + ", upvotes="
        + upvotes
        + ", topicId='"
        + topicId
        + '\''
        + ", topicName='"
        + topicName
        + '\''
        + '}';
  }
}
