package com.gitrekt.quora.models;

public class TrendingQuestions {
  private String questionId; // question id
  private String questionTitle; // question title
  private String body; // question body
  private int upvotes; // question upvotes count

  public String getQuestionId() {
    return questionId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public int getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(int upvotes) {
    this.upvotes = upvotes;
  }

  @Override
  public String toString() {
    return "TrendingQuestions{"
        + "questionId='"
        + questionId
        + '\''
        + ", title='"
        + questionTitle
        + '\''
        + ", body='"
        + body
        + '\''
        + ", upvotes="
        + upvotes
        + '}';
  }
}
