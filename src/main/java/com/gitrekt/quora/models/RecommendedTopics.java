package com.gitrekt.quora.models;

public class RecommendedTopics {
  private String topicId;
  private String topicName;

  public String getTopicId() {
    return topicId;
  }

  public void setTopicId(String topicId) {
    this.topicId = topicId;
  }

  public String getTopicName() {
    return topicName;
  }

  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }

  @Override
  public String toString() {
    return "RecommendedTopics{"
        + "topicId='"
        + topicId
        + '\''
        + ", topicName='"
        + topicName
        + '\''
        + '}';
  }
}
